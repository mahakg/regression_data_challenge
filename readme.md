# Schaeffler - Data Challenge

[![N|Solid](https://seeklogo.com/images/S/Schaeffler_group-logo-4E6570F9F0-seeklogo.com.gif)](https://upload.wikimedia.org/wikipedia/commons/5/5e/Schaeffler_Group_logo.gif)

This is my solution for the data challenge given by the team of Schaeffler (Datathon).

### Regression Models:
There are 6 ipython notebooks (Python 2.7) in total. You might need to install the notebook dependencies (incase of errors).

Techniques used:
 
  - For individual data, I tried to play around with multiple models and tried to tune the hyper-parameters manually and finally use the model which gives the least mean square error (In some cases its quite high, My assumption is - it might be due to the high target values essentially if the target is 1800 and the models predicts 1650 the MSE for one data point would be 150^2 and so on)
  
  - I used another criteria to see how good a model is fitting by using the Coefficient of determination r^2 that gives the measure of how well the model fits around the mean value.
  
  - Some models also use Normalization and scaling for input features
  
  - for testData3 specially, I tried to normalise the output feature as the target variable is very high. I chose to do this as the regression plot looked better if we scaled the output_feature against non-scaled output feature
  
  - Feature reduction - I removed the features which are highly collinear, I also used metrics of p-value and t-value to see the significance of individual fields.
  
  - For each file the individual models are saved in models folder.
  
  - The regression plots are saved in charts folder - They give a feeling how the model performs on training data - by splitting it into two parts. I've used threshold of 0.1 and 0.25.
  
  - To load a model (Please pass chosen_fields attribute as in every ipython file):
  
      >loaded_model = pickle.load(open('model/testData0.model', 'rb'))
      
      >result = loaded_model.predict(X[chosen_fields])
      
      >print(result)

### Clustering Model:
I have used KNN model as the target data is not very sparse and using the 3d plot we can visualise the number of possible clusters that can be possible from this data. I decided to use the number of clusters=5 by seeing the 3d plot. Its available in charts folder. I've also plotted the graph of KNN clustering output and saved in charts directory

Please let me know if anything does not work as explained or I missed out to add details about any particular thing.

Looking forward to your feedback.
