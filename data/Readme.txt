In the given files "testData<X>" are some columns.

The column "t" indicates the target.

Please try to build a model to predict "t" for the given data points.

Choose whatever tool you would like to use.

"testData6.csv" is no column "t" - try to find clusters and build a model which would be able to predict the cluster for new data points.

Please send me your results to my email: Lars.Heppert@schaeffler.com (or a link to a drop box folder with the results)

Your solution should include the scripts to build your model as well as some explanations for the steps you included in your analysis. The provided model will be tested with some other data points - it should generalize pretty well.